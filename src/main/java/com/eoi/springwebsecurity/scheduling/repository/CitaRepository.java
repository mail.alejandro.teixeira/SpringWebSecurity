package com.eoi.springwebsecurity.scheduling.repository;

import com.eoi.springwebsecurity.scheduling.entities.Cita;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CitaRepository extends JpaRepository<Cita, Integer> {




}