package com.eoi.springwebsecurity.websockets.entities;

import org.springframework.data.jpa.repository.JpaRepository;

public interface NotificacionRepository extends JpaRepository<Notificacion, Long> {


}